package fr.scholanova.eial.archdist.PresentationEjb;

import fr.scholanova.eial.archdist.PresentationEjbInterface.PresentationEjbInterface;
import javax.ejb.Remote;
import javax.ejb.Singleton;


@Remote(PresentationEjbInterface.class)
@Singleton
public class Calcul implements PresentationEjbInterface {

	@Override
	public int ajouter(int x, int y) {
		// TODO Auto-generated method stub
		return x+y;
	}
	
	@Override
	public double puissance(double nombre, double exposant) {
		// TODO Auto-generated method stub
		return Math.pow(nombre,exposant);
	}

}
